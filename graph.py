import random

class Graph:
    """
        Граф внутри будет представлен матрицой смежности.
        Социальный граф = Несвязанный граф (Это важно!)
    """

    def generate_matrix(self, count=100):
        """
            генерация матрицы смежности
            n - кол-во связей, по факту кол-во вершин
        """
        # генерируем n людей
        n = random.randint(count, int(count + count / 2))
        # Для простоты кода, создадим сперва пустую матрицу
        self.matrix = [[0 for i in range(0, n)] for i in range(0, n)]
        # Для генерации графа будем строить путь
        node_list = list() # Список вершин, у которых есть связи
        # Выберем сперва первую случайную вершину
        i = random.randint(0, n - 1)
        # Будем строить маршрут до тех пор, пока не наберем 
        # нужное кол-во вершин в счетчике
        while len(node_list) < count:
            # Выбераем куда пойдем
            j = random.randint(0, n - 1)
            # Проверим, удовлетворяет ли j-я вершина всем условиям
            while j == i or 3 <= sum(self.matrix[j]):
                # Если не удовлетворяет, выбираем другую
                # Хочу заметить, что это тупой способ выборки
                # Но при большом кол-ве вершин большая дисперсия
                # Так что норм
                j = random.randint(0, n - 1)
            # собственно строим связь
            self.matrix[i][j], self.matrix[j][i] = 1, 1
            i = j
            # Проверяем были ли у этой вершины связи
            if i not in node_list:
                # Если не было, то добавляем ее в наш список
                node_list.append(i)

    def find(self, index, n):
        """
            Поиск всех незнакомцев, 
            имеющих n общего друга с index участником
        """
        friends = list()
        non_friends = list()
        true_indexes = list() # подходящие под условия индексы
        for i in range(0, len(self.matrix)):
            # находим друзей и незнакомцев данного узла
            if index == i:
                continue
            elif self.matrix[index][i] == 1:
                friends.append(i)
            elif self.matrix[index][i] == 0:
                non_friends.append(i)
        # если друзей меньше, чем нужно найти, то все понятно
        if len(friends) < n:
            return []
        # перебираем всех незнакомцев
        for i in non_friends:
            mutual_count = 0
            # и всех друзей
            for j in friends:
                # Если незнакомец знает друга, то увеличиваем счетчик
                if self.matrix[i][j]:
                    mutual_count += 1
            # Если кол-во нужное, добавляем
            if mutual_count == n:
                true_indexes.append(i)
        return true_indexes

    def __len__(self):
        return len(self.matrix)
