import sys
from PyQt5 import QtWidgets
from gui.graph_ui import GraphUI


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = GraphUI()
    window.show()
    app.exec_()

if __name__ == "__main__":
    main()
