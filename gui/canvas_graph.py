import math
import random
from PyQt5 import QtWidgets, QtGui, QtCore


class CanvasGraph(QtWidgets.QLabel):
    class DrawMatrixLine:
        """Для рисования линии"""
        def __init__(self, x0, y0, R, n):
            self.x0 = x0
            self.y0 = y0
            self.R = R
            self.n = n

        def draw(self, painter, i, j):
            r1 = i + math.pi / self.n;
            x1 = self.x0 + math.sin(r1) * self.R;
            y1 = self.y0 + math.cos(r1) * self.R;
            r2 = j + math.pi / self.n;
            x2 = self.x0 + math.sin(r2) * self.R;
            y2 = self.y0 + math.cos(r2) * self.R;
            painter.drawLine(x1, y1, x2 , y2)

    matrix = None
    show_flag = False
    to_top = None
    from_top = None

    def set_matrix(self, matrix):
        self.matrix = matrix
        self.to_top = None
        self.from_top = None
        # перерисовываем canvas
        self.update()

    def set_tops_to_paint(self, to_top, from_top):
        self.to_top = to_top
        self.from_top = from_top
        self.update()

    def set_show_flag(self, flag):
        self.show_flag = flag

    def paintEvent(self, event):
        super().paintEvent(event)
        if not self.matrix:
            return
        painter = QtGui.QPainter(self)
        painter.setPen(QtGui.QPen(QtCore.Qt.black, 3.0))
        # вычислим радиус окружности 
        R = min(self.width(), self.height()) / 2.1
        # вычисляем центр окружности
        x0 = self.width() / 2
        y0 = self.height() / 2
        n = len(self.matrix)
        draw_line = self.DrawMatrixLine(x0, y0, R, n)
        # Рисуем вершины
        for i in range(0, n):
            r = i + math.pi / n
            x = x0 + math.sin(r) * R;
            y = y0 + math.cos(r) * R;
            painter.drawPoint(x, y)
            if self.show_flag:
                painter.drawText(x, y, str(i))
        # Поменяем толщину линии 
        painter.setPen(QtGui.QPen(QtCore.Qt.black, 0.5))
        # рисуем связи
        for i in range(0, n):
            for j in range (i, n):
                if self.matrix[i][j]:
                    draw_line.draw(painter, i, j)
        if not self.to_top or not self.from_top:
            return
        for next_top in self.from_top:
            # вершины, до которых есть путь от from_top
            pathes = [
                i 
                for i in range(0, len(self.matrix[next_top])) 
                if self.matrix[next_top][i]
            ]
            for path in pathes:
                if self.matrix[path][self.to_top]:
                    painter.setPen(
                        QtGui.QPen(
                            QtGui.QColor(
                                random.randrange(255),
                                random.randrange(255),
                                random.randrange(255)),
                            0.7
                        )
                    )
                    draw_line.draw(painter, next_top, path)
                    draw_line.draw(painter, path, self.to_top)
