from PyQt5 import QtWidgets
from PyQt5 import QtGui
from gui.design import Ui_MainWindow
from graph import Graph


class GraphUI(QtWidgets.QMainWindow, Ui_MainWindow):
    """Класс постоения GUI интерфейса"""

    def __init__(self):
        super().__init__()
        self.graph =  Graph()
        # Инициализируем UI
        self.setupUi(self)
        # Прикрепляем на нажатие кнопки событие
        self.genGraphBtn.clicked.connect(self.generate_graph)
        self.listWidget.currentItemChanged.connect(self.show_unfriends)

    def generate_graph(self):
        def fill_table(self):
            """Заполнить таблицу"""
            n = len(self.graph)
            self.tableWidget.setRowCount(n);
            self.tableWidget.setColumnCount(n);
            for i in range(0, n):
                # Устанавливаем заголовки 
                self.tableWidget.setHorizontalHeaderItem(
                    i,
                    QtWidgets.QTableWidgetItem(str(i))
                )
                self.tableWidget.setVerticalHeaderItem(
                    i,
                    QtWidgets.QTableWidgetItem(str(i))
                )
                # Сами ячейки таблицы
                for j in range(0, n):
                    self.tableWidget.setItem(
                        i,
                        j,
                        QtWidgets.QTableWidgetItem(str(self.graph.matrix[i][j]))
                    )
        def fill_list(self):
            """Заполнить список незнакомцев"""
            count_friend = self.countEdit.value()
            for i in range(0, len(self.graph)):
                next_list = self.graph.find(i, count_friend)
                if next_list:
                    QtWidgets.QListWidgetItem(
                        '{i}: {next_list}'.format(i=i, next_list=next_list),
                        self.listWidget
                    )
        # генерируем новую матрицу смежности
        self.graph.generate_matrix(self.countVerticesBox.value())
        # Очищаем предыдущую матрицу, если была
        self.tableWidget.clear()
        # Далее заполняем таблицу данными из матрицы
        fill_table(self)
        # перерисуем Canvas
        self.canvasLabel.set_show_flag(self.showNumberCheckBox.isChecked())
        self.canvasLabel.set_matrix(self.graph.matrix)
        # Очищаем старый список незнакомцев
        self.listWidget.clear()
        # Заполняем список незнакомцев
        if self.countEdit.value():
            fill_list(self)

    def show_unfriends(self):
        if self.listWidget.currentItem() is None:
            return
        index = int(self.listWidget.currentItem().text().split(':')[0])
        values = self.graph.find(index, self.countEdit.value())
        self.canvasLabel.set_tops_to_paint(to_top=index, from_top=values)
